import React, { useState } from 'react'
import styles from '../styles/index.styl'
import Gallery from '../components/Gallery/Gallery'

export const GalleryContext = React.createContext()

const Index = () => {

  const [galleryRoute, setGalleryRoute] = useState({
    path: null
  })

  return (
    <div className={styles.page_container} >
      <div className={styles.main} >
        <div className={styles.flex_container} >
          <p className={styles.name} >Maël Larcher</p>
          <div className={styles.typing_container} >
            <p className={styles.typing}>Photographie</p>
          </div>
        </div>
      </div>
      <div className={styles.arrow_container} >
        <a href="#anchor">
        <img src="http://image.flaticon.com/icons/svg/3/3907.svg" id={styles.arrow} className={styles.bounce}></img>
        </a>
      </div>
      <div className={styles.second_part} id="anchor" >
        <p className={styles.title} >Portfolio</p>
        <GalleryContext.Provider value={{
          path: galleryRoute.path, 
          setRoute: newPath => setGalleryRoute({path: newPath})
        }} >
          <Gallery />
        </GalleryContext.Provider>
      </div>
    </div>
  )
}

export default Index