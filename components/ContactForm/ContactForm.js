import React, {useState} from 'react';
import axios from 'axios'
import styles from './ContactForm.styl'

const ContactForm = () => {

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    message: ""
  })

  const [sendStatus, setSendStatus] = useState(null)

  const updateField = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = e => {
    e.preventDefault()
    setSendStatus('Envoie en cours ...')
    
    axios({
      method: "POST",
      url: "/api/send",
      data: formData
    }).then(res => {
      console.log(res)
      if (res.status === 200) {
        setSendStatus('Message envoyé !')
      } else if (res.status === 500) {
        setSendStatus('Une erreur est survenue.')
      }
    })

  }
 
  return (
    <div className={styles.form_container} >
      <form onSubmit={handleSubmit} method="post">
        <p className={styles.form_title} >Envoyer un message</p>
        <input 
          type="text" 
          className={styles.line_input} 
          placeholder='Nom complet'
          name='name' 
          value={formData.name}
          onChange={updateField}
        />
        <input 
          type="email" 
          className={styles.line_input} 
          placeholder='Email'
          name='email' 
          value={formData.email}
          onChange={updateField}
        />
        <textarea
          type="text" 
          className={styles.area_input} 
          placeholder='Message'
          name='message' 
          value={formData.message}
          onChange={updateField}
        />

        <button type='submit' className={styles.submit} >Envoyer</button>
        {
          sendStatus ? (
            <p className={styles.status}> {sendStatus} </p>
          ) : null
        }
      </form>
    </div>
  );
};

export default ContactForm;