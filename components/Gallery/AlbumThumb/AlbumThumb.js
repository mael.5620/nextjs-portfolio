import React, { useContext } from 'react';
import css from './AlbumThumb.styl'
import { GalleryContext } from '../../../pages/index'

const AlbumThumb = ({ album }) => {
  const route = useContext(GalleryContext)

  return (
    <div className={css.img_wrap} onClick={() => route.setRoute(album.path)} >
      <img src={album.sources[0]} alt="" className={css.thumb} />
      <p className={css.libelle} > {album.libelle} </p>
    </div>
  )
}
export default AlbumThumb;