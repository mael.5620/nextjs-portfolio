import React, { useState, useContext } from 'react';
import { map } from 'lodash'
import css from './Gallery.styl'
import images from '../../public/images/index'
import Album from './Album/Album';
import { GalleryContext } from '../../pages/index'
import AlbumThumb from './AlbumThumb/AlbumThumb';

const Gallery = () => {

  const route = useContext(GalleryContext)

  return (
    <div className={css.gallery_container} >
      {
        route.path ? (
          <Album images={images[route.path]} />
        ) : (
          <div className={css.album_list} >
            {
              map(images, album => (
                <AlbumThumb album={album} />
            ))
            }
          </div>
        )
      }
    </div>
  );
};

export default Gallery;