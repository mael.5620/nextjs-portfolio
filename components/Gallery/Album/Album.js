import React, { UseState, useContext } from 'react'
import { map } from 'lodash'
import Image from './Image/Image'
import { GalleryContext } from '../../../pages/index'
import css from './Album.styl'

const Album = ({ images }) => {

  const route = useContext(GalleryContext)

  return (
    <>
      <p className={css.libelle} onClick={() => route.setRoute(null)} > ← /   {images.libelle} </p>
      <div className={css.img_container} >
        {
          map(images.sources, img => (
            <Image src={img} />
          ))
        }
      </div>
    </>
  )
}

export default Album