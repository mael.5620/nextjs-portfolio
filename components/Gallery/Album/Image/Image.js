import React, { UseState } from 'react'
import css from './Image.styl'

const Image = ({ src }) => {

  return (
    <>
      <img src={src} alt=""/>
    </>
  )
}

export default Image