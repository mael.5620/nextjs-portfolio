export default {
  hennebont: {
    libelle: "Hennebont",
    path: "hennebont",
    sources: [
      "/images/hennebont/IMG_1300.jpg",
      "/images/hennebont/IMG_1358.jpg",
      "/images/hennebont/IMG_1317.jpg",
      "/images/hennebont/IMG_1348.jpg"
    ]
  },
  velo: {
    libelle: "Vélo vintage",
    path: "velo",
    sources: [
      "/images/velo/IMG_4467.jpg",
      "/images/velo/IMG_4468.jpg",
      "/images/velo/IMG_4469.jpg",
      "/images/velo/IMG_4479.jpg"
    ]
  },
  italie: {
    libelle: "Italie",
    path: "italie",
    sources: [
      "/images/italie/IMG_9293.jpg",
      "/images/italie/IMG_9297.jpg",
      "/images/italie/IMG_9323.jpg",
      "/images/italie/IMG_9340.jpg",
      "/images/italie/IMG_9364-2.jpg",
      "/images/italie/IMG_9463.jpg",
      "/images/italie/IMG_9508.jpg",
      "/images/italie/IMG_9512.jpg",
      "/images/italie/IMG_9516.jpg"
    ]
  }
}